---
title: "Fresadoras - American Vial Arrecifes"
layout: "products-list-vial"
draft: false

products:
  enable: true
  subtitle: "Fresadoras"
  title: "Vial Fresadoras"
  description: "Fresadoras, equipos viales en alquiler y venta."
  products_items:
  - name: "Fresadora XM 200k"
    image: "images/maquinas-categorias/vial/vial_fresadoras.png"
    link: "./maquinas-viales/fresadora/xm200k/"
  - name: "Fresadora XM 1003k"
    image: "images/maquinas-categorias/vial/vial_fresadoras.png"
    link: "./maquinas-viales/fresadora/xm1003k/"

---