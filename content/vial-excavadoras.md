---
title: "Excavadoras - American Vial Arrecifes"
layout: "products-list-vial"
draft: false

products:
  enable: true
  subtitle: "Excavadoras"
  title: "Vial Excavadoras"
  description: "Excavadoras, equipos viales en alquiler y venta."
  products_items:
  - name: "Excavadora XCMG XE80D"
    image: "images/maquinas-categorias/vial/vial_excavadoras.png"
    link: "./maquinas-viales/excavadora/excavadora-xcmg-xe80d/"
  - name: "Excavadora XCMG XE150BR"
    image: "images/maquinas-categorias/vial/vial_excavadoras.png"
    link: "./maquinas-viales/excavadora/excavadora-xcmg-xe150br/"
  - name: "Excavadora XCMG XE370BR"
    image: "images/maquinas-categorias/vial/vial_excavadoras.png"
    link: "./maquinas-viales/excavadora/excavadora-xcmg-xe370br/"

---