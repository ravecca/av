---
title: "Trompos hormigoneros - American Vial Arrecifes"
layout: "products-list-vial"
draft: false

products:
  enable: true
  subtitle: "Trompos hormigoneros"
  title: "Vial Trompos hormigoneros"
  description: "Trompos hormigoneros, equipos viales en alquiler y venta."
  products_items:
  - name: "Trompos hormigoneros"
    image: "images/maquinas-categorias/vial/vial_trompos-hormigoneros.png"
    link: "./maquinas-viales/trompos-hormigoneros/trompo/"


---