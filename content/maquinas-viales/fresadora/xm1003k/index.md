---
title: "Fresadora - XM 1003K"
layout: "single-vial"
draft: false

modelo: "XM 1003K"
medidas:
peso: "Peso de Funcionamiento:** 14.500 Kg"
detalles: "
**Ancho Máximo de Fresado:** 1020 mm<br/>
**Máxima Profundidad de Fresado:** 200 mm<br/>
**Cantidad de herramientas:** 84 Unidades<br/>
**Diámetro de Tambor:** 660 mm<br/>
**Potencia Nominal:** 168 Hp<br/>
**Revoluciones:** 2200 rpm<br/>
**Velocidad de Funcionamiento:** 0-13 m/min<br/>
**Velocidad:** 0-13 Km/h<br/>
**Ancho Cinta Transportadora:** 400 mm<br/>
**Medida Cinta Transportadora:** 6428x700x610 mm<br/>
**Tipo de Traslación:** Neumáticos / Orugas (según configuración)
"

---

{{< gallery >}} 