---
title: "Fresadora - XM 200KII"
layout: "single-vial"
draft: false

modelo: "XM 200KII"
medidas:
peso: "Peso de Funcionamiento - 32.500 Kg"
detalles: "
**Ancho Máximo de Fresado:** 2000 mm<br/>
**Máxima Profundidad de Fresado:** 300 mm<br/>
**Cantidad de Herramientas:** 168 unidades<br/>
**Diámetro de Tambor:** 980 mm<br/>
**Revoluciones:** 2100 rpm<br/>
**Velocidad de Funcionamiento:** 0-30 m/min<br/>
**Velocidad:** 0-4.5 Km/h<br/>
**Ancho Cinta Transportadora:** 400 mm<br/>
**Medida Cinta Transportadora:** 6428x700x610 mm<br/>
**Tipo de Traslación:** Orugas
"

---

{{< gallery >}} 