---
title: "Grúas sobre camión - American Vial Arrecifes"
layout: "products-list-vial"
draft: false

products:
  enable: true
  subtitle: "Grúas sobre camión"
  title: "Vial Grúas sobre camión"
  description: "Grúas sobre camión, equipos viales en alquiler y venta."
  products_items:
  - name: "Grúas XCT60"
    image: "images/maquinas-categorias/vial/vial_gruas.png"
    link: "./maquinas-viales/gruas/gruas-sobre-camion/xct60/index.md"
   
---