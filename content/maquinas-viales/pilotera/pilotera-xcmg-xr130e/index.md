---
title: "Pilotera XCMG XR130E"
layout: "single-vial"
draft: false

modelo: "Pilotera XCMG XR130E"
medidas:
peso: "Peso Operativo 45000Kg"
detalles: "
**Máximo Diámetro de Perforación:** 1300 /1580 mm<br/>
**Máxima Profundidad de Perforación:** 50 m<br/>
**Rango de Operación Segura (desde el centro de rotación):** 3150 - 3550 mm<br/>
**Largo Total:** 7122 mm<br/>
**Ancho Total:** 4200 mm<br/>
**Alto Total:** 17250 mm
"

---

{{< gallery >}} 