---
title: "Pilotera XCMG XR320D"
layout: "single-vial"
draft: false

modelo: "Pilotera XCMG XR320D"
medidas:
peso: "Peso Operativo 92800Kg"
detalles: "
**Potencia Nominal:** 400Hp@2100rpm<br/>
**Máx. Diám. Perforación:** 2500mm<br/>
**Prof. Máxima Perforación:** 90m<br/>
"

---

{{< gallery >}} 