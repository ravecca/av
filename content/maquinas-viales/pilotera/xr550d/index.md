---
title: "Pilotera XCMG XR550D"
layout: "single-vial"
draft: false

modelo: "Pilotera XCMG XR550D"
medidas:
peso: "Peso Operativo 180000Kg"
detalles: "
**Potencia Nominal:** 599Hp@2100rpm<br/>
**Máx. Diám. Perforación:** 3500mm<br/>
**Prof. Máxima Perforación:** 132m
"

---

{{< gallery >}} 