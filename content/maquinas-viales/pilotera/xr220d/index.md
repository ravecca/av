---
title: "Pilotera XCMG XR220D"
layout: "single-vial"
draft: false

modelo: "Pilotera XCMG XR220D"
medidas:
peso: "Peso Operativo 76000Kg"
detalles: "
**Potencia Nominal:** 324Hp@2100rpm<br/>
**Máx. Diám. Perforación:** 2000mm<br/>
**Prof. Máxima Perforación:** 68m<br/>
"

---

{{< gallery >}} 