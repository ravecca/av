---
title: "Pilotera XCMG XR460D"
layout: "single-vial"
draft: false

modelo: "Pilotera XCMG XR460D"
medidas:
peso: "Peso Operativo 158000Kg"
detalles: "
**Potencia Nominal:** 599Hp@2100rpm<br/>
**Máx. Diám. Perforación:** 3000mm<br/>
**Prof. Máxima Perforación:** 120m<br/>
"

---

{{< gallery >}} 