---
title: "Trompos hormigoneros"
layout: "single-vial"
draft: false

modelo: 
medidas:
peso: 
detalles: "
**GO4V**<br/>
**Tipo de Chasis:** XCMG<br/>
**Tracción:** 4x2<br/>
**Capac. de Trompo:** 4m3<br/><br/>
**GO8V**<br/>
**Tipo de Chasis:** XCMG/SINOTRUCK<br/>
**Tracción:** 6x4<br/>
**Capac. de Trompo:** 8m3<br/><br/>
**GO12V**<br/>
**Tipo de Chasis:** XCMG/SINOTRUCK<br/>
**Tracción:** 6x4<br/>
**Capac. de Trompo:** 12m3<br/><br/>
**GO16V**<br/>
**Tipo de Chasis:** SINOTRUCK<br/>
**Tracción:** 8x4<br/>
**Capac. de Trompo:** 16m3
"

---

{{< gallery >}} 