---
title: "Rodillo compactador XS123PD BR"
layout: "single-vial"
draft: false

modelo: "Rodillo compactador XS123PD BR"
medidas:
peso: 
detalles: "
**Peso Operativo:** 12000/14000Kg<br/>
**Potencia Nominal:** 130Hp@2200rpm<br/>
**Ancho de Tambor:** 1523mm<br/><br/>

**CV83U**<br/>
**Peso Operativo:** 8000Kg<br/>
**Potencia Nominal:** 99Hp@2200rpm<br/>
**Ancho de Tambor:** 1523mm<br/><br/>

**XS163**<br/>
**Peso Operativo:** 16000Kg<br/>
**Potencia Nominal:** 140Hp@1800rpm<br/>
**Ancho de Tambor:** 1523mm<br/><br/>

**XS203**<br/>
**Peso Operativo:** 20000Kg<br/>
**Potencia Nominal:** 190Hp@2200rpm<br/>
**Ancho de Tambor:** 1600mm<br/><br/>

**XS223**<br/>
**Peso Operativo:** 23000Kg<br/>
**Potencia Nominal:** 220Hp@2000rpm<br/>
**Ancho de Tambor:** 1600mm<br/>
"

---

{{< gallery >}} 