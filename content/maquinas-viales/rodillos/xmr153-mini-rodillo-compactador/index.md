---
title: "Mini rodillo compactador "
layout: "single-vial"
draft: false

modelo: "Mini rodillo compactador"
medidas:
peso: 
detalles: "
**XMR153**<br/>
**Peso Operativo:** 1680Kg<br/>
**Potencia Nominal:** 23.6Hp@2500rpm<br/>
**Medidas:** 2210x1550x985mm<br/><br/>
**XMR203**<br/>
**Peso Operativo:** 2000Kg<br/>
**Potencia Nominal:** 23.6Hp@2200rpm<br/>
**Medidas:** 2370x1090x1575mm<br/><br/>
**XMR303**<br/>
**Peso Operativo:** 3100Kg<br/>
**Potencia Nominal:** 38Hp@1800rpm<br/>
**Medidas:** 2675x1318x2680mm<br/><br/>
**XMR403**<br/>
**Peso Operativo:** 4000Kg<br/>
**Potencia Nominal:** 48Hp@2200rpm<br/>
**Medidas:** 2720x1416x2776mm
"

---

{{< gallery >}} 