XMR203
Peso Operativo: 2000Kg
Potencia Nominal: 23.6Hp@2200rpm
Medidas: 2370x1090x1575mm

XMR303
Peso Operativo: 3100Kg
Potencia Nominal: 38Hp@1800rpm
Medidas: 2675x1318x2680mm

XMR403
Peso Operativo: 4000Kg
Potencia Nominal: 48Hp@2200rpm
Medidas: 2720x1416x2776mm