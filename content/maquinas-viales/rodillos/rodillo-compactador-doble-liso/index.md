---
title: "Rodillo compactador doble liso"
layout: "single-vial"
draft: false

modelo: "Rodillo compactador doble liso"
medidas:
peso: 
detalles: "
**XD83<br/>
**Peso Operativo:** 8500Kg<br/>
**Potencia Nominal:** 100Hp@2300rpm<br/>
**Ancho de Trabajo:** 1680mm<br/><br/>
**XD102**<br/>
**Peso Operativo:** 10000Kg<br/>
**Potencia Nominal:** 100Hp@2300rpm<br/>
**Ancho de Trabajo:** 1750mm<br/><br/>
**XD123**<br/>
**Peso Operativo:** 12300Kg<br/>
**Potencia Nominal:** 160Hp@2300rpm<br/>
**Ancho de Trabajo:** 2130mm
"

---

{{< gallery >}} 