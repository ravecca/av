---
title: "Rodillos neumáticos"
layout: "single-vial"
draft: false

modelo: "Rodillos neumáticos"
medidas:
peso: 
detalles: "
**XP103<br/>
**Peso Operativo:** 9300Kg<br/>
**Potencia Nominal:** 125Hp@1800rpm<br/>
**Ancho de Trabajo:** 2050mm<br/><br/>
**XP203**<br/>
**Peso Operativo:** 11750Kg<br/>
**Potencia Nominal:** 115Hp@1800rpm<br/>
**Ancho de Trabajo:** 2250mm<br/><br/>
**XP263**<br/>
**Peso Operativo:** 15000Kg<br/>
**Potencia Nominal:** 180Hp@1800rpm<br/>
**Ancho de Trabajo:** 2750mm
"

---

{{< gallery >}} 