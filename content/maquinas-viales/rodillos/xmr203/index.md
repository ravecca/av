---
title: "Rodillo compactador XMR203"
layout: "single-vial"
draft: false

modelo: "Rodillo compactador XMR203"
medidas:
peso: 
detalles: "
**XMR203**<br/>
**Peso Operativo:** 2000Kg<br/>
**Potencia Nominal:** 23.6Hp@2200rpm<br/>
**Medidas:** 2370x1090x1575mm<br/><br/>
**XMR303**<br/>
**Peso Operativo:** 3100Kg<br/>
**Potencia Nominal:** 38Hp@1800rpm<br/>
**Medidas:** 2675x1318x2680mm<br/><br/>
**XMR403**<br/>
**Peso Operativo:** 4000Kg<br/>
**Potencia Nominal:** 48Hp@2200rpm<br/>
**Medidas:** 2720x1416x2776mm
"

---

{{< gallery >}} 