---
title: "Cargadora Frontal - XCMG XE80D"
layout: "single-vial"
draft: false

modelo: "XCMG XE80D"
medidas:
peso: "Peso de operación: 7460 kg"
detalles: "
**Distancia Max. excavación:** 6295 mm<br/>
**Profundidad de escavación:** 4160 mm<br/>
**Balde de excavación:** 0.3 m3 de uso general<br/>
**Largo total:** 6155 mm<br/>
**Ancho total:** 2200 mm<br/>
**Alto total:** 2618 mm
"

---

{{< gallery >}} 