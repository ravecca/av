---
title: "Excavadora - XCMG XE150BR"
layout: "single-vial"
draft: false

modelo: "XCMG XE150BR"
medidas:
peso: "Peso Operativo: 14400 kg"
detalles: "
**Modelo del motor:** 4BTAA3.9<br/>
**Potencia del motor:** 120 Hp / 2200 rpm<br/>
**Máx. fuerza de arrastre:** 103 kN<br/>
**Máx. fuerza de arranque:** 138 kN<br/>
**Radio mínimo de giro:** 2325 mm<br/>
**Máx. altura de excavación:** 6181 mm<br/>
**Profundidad de escavación:** 8641 mm<br/>
**Balde de excavación:** 0.6 m3<br/>
**Peso de operación:** 14400 kg<br/>
**Largo total:** 7801 mm<br/>
**Ancho total:** 2590 mm<br/>
**Alto total:** 2850 mm<br/>
**Dimensión del Brazo:** 2950 mm<br/>
"

---

{{< gallery >}} 