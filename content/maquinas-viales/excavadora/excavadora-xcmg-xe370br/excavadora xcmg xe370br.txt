Detalles
Modelo del motor: Cummins QSC 8.3
Potencia del motor: 260 Hp
Radio mínimo de giro: 4433 mm
Máx. altura de excavación: 10449 mm
Profundidad de escavación: 6764 mm
Balde de excavación: 1.6 / 2.4 m3
Peso de operación: 37,800 Kg
Largo total: 11184 mm
Ancho total: 3290 mm
Alto total: 3530 mm
Fuerza de desarticulación de pala: 263 kN
Fuerza de desarticulación de brazo: 225 kN
Fuerza de tracción: 285 kN