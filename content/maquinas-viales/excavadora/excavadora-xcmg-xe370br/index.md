---
title: "Excavadora - XCMG LW370BR"
layout: "single-vial"
draft: false

modelo: "XCMG LW370BR"
medidas:
peso: "Peso Operativo: 37,800 kg"
detalles: "
**Modelo del motor:** Cummins QSC 8.3<br/>
**Potencia del motor:** 260 Hp<br/>
**Radio mínimo de giro:** 4433 mm<br/>
**Máx. altura de excavación:** 10449 mm<br/>
**Profundidad de escavación:** 6764 mm<br/>
**Balde de excavación:** 1.6 / 2.4 m3<br/>
**Largo total:** 11184 mm<br/>
**Ancho total:** 3290 mm<br/>
**Alto total:** 3530 mm<br/>
**Fuerza de desarticulación de pala:** 263 kN<br/>
**Fuerza de desarticulación de brazo:** 225 kN<br/>
**Fuerza de tracción:** 285 kN<br/>
"

---

{{< gallery >}} 