---
title: "Topadora TY165-3 - American Vial Arrecifes"
layout: "single-vial"
draft: false

traccion: ""
motortipo: "QSL8.9"
potencia: "169 Hp / 1850 rpm"
desplazamiento: ""
velocidadrotacion: ""
marchas: ""
velocidad: ""
freno: ""
fuerza: ""
suspension: ""
levante: ""
largo: ""
ancho: "Ancho de Vía - 1880 mm"
altitud: ""
peso: "16.729 Kg"
detalles: "
**Máx. Profundidad de Excavación:** 582mm<br/>
**Presión Sobre el Suelo** - 55 kPa<br/>
**Ancho de la Hoja** - 3324 mm<br/>
**Máx. Profundidad de Excavación** - 592 mm<br/>
**Dimensiones Totales** - 5037 x 3270 x 3077 mm<br/>
**Números de Rodillos de Cadena** - 7<br/>
**Ancho de Zapato** - 560 mm<br/>
**Velocidad Máxima** - 10.9 km/h<br/>
**Máx. Esfuerzo de Torsión** - 880 Nm<br/>
"
---

{{< gallery >}} 