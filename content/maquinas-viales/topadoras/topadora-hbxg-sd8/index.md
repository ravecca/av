---
title: "Topadora HBXG SD8 - American Vial Arrecifes"
layout: "single-vial"
draft: false

traccion: ""
motortipo: "NYA855 - C400"
potencia: "326 Hp / 1900 rpm"
desplazamiento: ""
velocidadrotacion: ""
marchas: ""
velocidad: ""
freno: ""
fuerza: ""
suspension: ""
levante: ""
largo: ""
ancho: "Ancho de Vía - 2083 mm"
altitud: ""
peso: "37.400 Kg"
detalles: "
**Máx. Profundidad de Excavación:** 582mm<br/>
**Presión Sobre el Suelo** - 93 kPa<br/>
**Ancho de la Hoja** - 3940 mm<br/>
**Máx. Profundidad de Excavación** - 582 mm<br/>
**Dimensiones Totales** - 7751x3940x3549 mm<br/>
**Números de Rodillos de Cadena** - 8<br/>
**Ancho de Zapato** - 560 mm<br/>
**Velocidad Máxima** - 11.9 km/h<br/>
"
---

{{< gallery >}} 