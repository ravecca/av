---
title: "Topadora HBXG SD7 - American Vial Arrecifes"
layout: "single-vial"
draft: false

traccion: ""
motortipo: "NTA855-C280"
potencia: "235 Hp"
desplazamiento: ""
velocidadrotacion: ""
marchas: ""
velocidad: ""
freno: ""
fuerza: ""
suspension: ""
levante: ""
largo: ""
ancho: "Ancho de Vía: 1980mm"
altitud: ""
peso: "23.800 Kg"
detalles: "
**Dozer:** Inclinación<br/>
**Máx. Fuerza de Tracción:** 209.92 kN<br/>
**Capacidad de Goteo:** 8.4m3<br/>
**Máx. Profundidad de Excavación:** 498mm<br/>
**Tipo Bomba:** High pressure gear pump
"
---

{{< gallery >}} 