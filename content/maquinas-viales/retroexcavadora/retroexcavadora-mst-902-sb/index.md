---
title: "Retroexcavadora MST 902 SB"
layout: "single-vial"
draft: false

modelo: "Retroexcavadora MST 902 SB"
medidas:
peso: 
detalles: "
**Tracción 4x4:** Power Shift<br/>
**Modelo del Motor:** Perkins 1104C-44t<br/>
**Altura Máxima de Descarga:** 2789 mm<br/>
**Profundidad de Excavación:** 4.5/5.4 m<br/>
**Capacidad Balde delantero:** 1.2m3<br/>
**Capacidad Balde trasero:** 0.35m3
"

---

{{< gallery >}} 