---
title: "Mini cargadora WECAN 1100"
layout: "single-vial"
draft: false

modelo: "Mini cargadora WECAN 1100"
medidas:
peso: 
detalles: "
**Modelo del motor:** A498BZG engine<br/>
**Potencia:** 73 Hp<br/>
**Flujo nominal:** 65.5 L/min<br/>
**Capacidad nominal:** 1100Kg<br/>
**Alto flujo:** 128 L/min<br/>
**Altura de descarga:** 2465 mm<br/>
**Distancia de descarga:** 610 mm<br/>
**Tipo de potencia:** Diesel<br/>
**Tipo de control:** Manual control<br/>
**Longitud total con cubo:** 3623 mm<br/>
**Ancho total con cubeta:** 1980 mm<br/>
**Altura total:** 2028 mm<br/>
**Max. Velocidad:** 22.4 Km/h<br/>
**Max. Carga: **2200 Kg<br/>
**Tipo de Elevación:** Radius<br/>
**Velocidad nominal:** 2500 rpm<br/>
**Desplazamiento:** 3.17 L
"

---

{{< gallery >}} 