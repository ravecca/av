---
title: "Mini cargadora WECAN XT650"
layout: "single-vial"
draft: false

modelo: "Mini cargadora WECAN XT650"
medidas:
peso: "Peso de la maquina con balde: 2900 kg"
detalles: "
**Modelo del motor:** Xinchai C490BPG-4<br/>
**Potencia de motor:** 50 Hp<br/>
**Flujo nominal:** 60 L/min<br/>
**Max. Velocidad:** 11.2 Km/h<br/>
**Carga Máxima:** 1300 kg<br/>
**Tipo de Elevación:** Radial<br/>
**Desplazamiento:** 2.54 L<br/>
**Capacidad de balde:** 0.45 m³<br/>
**Altura maxima de trabajo sobre balde:** 3970 mm<br/>
**Fuerza de elevación de balde:** 1500 kg<br/>
**Máximo Torque:** 156 Nm<br/>
**Cadena:** 80 H<br/>
**Operación de Carga:** 650Kg<br/>
**Presión de Sistema Hidráulico:** 160 bar<br/>
**Medida de Rueda:** 10-16.5<br/>
**Número de Cilindros:** 4
"

---

{{< gallery >}} 