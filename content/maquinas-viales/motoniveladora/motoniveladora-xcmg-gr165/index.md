---
title: "Moto niveladora XCMG GR165"
layout: "single-vial"
draft: false

modelo: "Moto niveladora XCMG GR165"
medidas:
peso: 
detalles: "
**Motor:** Cummins 6BTA5.9<br/>
**Dimensión del Equipo:** 8900x2625x3470 cm<br/>
**Mínimo Radio de Giro:** 7,3 m<br/>
**Máxima Altura Sobre el Suelo:** 450 mm<br/>
**Máxima Altura de Corte:** 500 m<br/>
**Máximo Angulo de Posición Hoja Topadora:** 90º<br/>
"

---

{{< gallery >}} 