---
title: "Moto niveladora XCMG GR215"
layout: "single-vial"
draft: false

modelo: "Moto niveladora XCMG GR215"
medidas:
peso: 
detalles: "
**Modelo:** QSB6.7-C220-31<br/>
**Peso Operacional:** 16.5 KG<br/>
**Potencia:** 220 Hp<br/>
**Velocidad de anvance:** 5,8,11,19,23,38 km/h<br/>
**Velocidad de retroceso:** 5,11,23 km/h<br/>
**Fuerza de tracción:** ≥85 kN<br/>
**Presión del sistema hidráulico:** ±20 Mpa<br/>
**Angulo de rotación:** 360°<br/>
**Largo x alto:** 4200 x 610 mm<br/>
**Máxima profundidad de pala:** 500 mm<br/>
**Ángulo de corte:** 28-70°
"

---

{{< gallery >}} 