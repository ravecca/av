Detalles
Motor: Cummins
Modelo: QSB6.7 - Diesel
Peso Operacional: 16100 - 17100 KG
Potencia: 193 Hp
Velocidad de anvance: 5,8,11,19,23,38 km/h
Velocidad de retroceso: 5,11,23 km/h
Fuerza de tracción: 89kN
Presión del sistema hidráulico: 18.5 Mpa
Angulo de rotación: 360°
Largo total: 8900mm
Ancho total: 2625mm
Alto total: 3420mm
Ángulo de corte: 40 adelante / 5 atrás°
Elevación máxima del suelo: 450mm
