---
title: "Moto niveladora XCMG GR1803 BR"
layout: "single-vial"
draft: false

modelo: "Moto niveladora XCMG GR1803 BR"
medidas:
peso: 
detalles: "
**Motor:** Cummins<br/>
**Modelo:** QSB6.7 - Diesel<br/>
**Peso Operacional:** 16100 - 17100 KG<br/>
**Potencia:** 193 Hp<br/>
**Velocidad de anvance:** 5,8,11,19,23,38 km/h<br/>
**Velocidad de retroceso:** 5,11,23 km/h<br/>
**Fuerza de tracción:** 89kN<br/>
**Presión del sistema hidráulico:** 18.5 Mpa<br/>
**Angulo de rotación:** 360°<br/>
**Largo total:** 8900mm<br/>
**Ancho total:** 2625mm<br/>
**Alto total:** 3420mm<br/>
**Ángulo de corte:** 40 adelante / 5 atrás°<br/>
**Elevación máxima del suelo:** 450mm<br/>
"

---

{{< gallery >}} 