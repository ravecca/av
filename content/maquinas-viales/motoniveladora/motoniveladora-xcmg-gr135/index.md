---
title: "Moto niveladora XCMG GR135"
layout: "single-vial"
draft: false

modelo: "Moto niveladora XCMG GR135"
medidas:
peso: 
detalles: "
**Motor:** Cummins 6BT5.9-C<br/>
**Largo Total:** 8015 mm<br/>
**Ancho Total:** 2380 mm<br/>
**Alto total:** 3050 mm<br/>
**Ancho de la Pala:** 3710 mm<br/>
**Alto de la Pala:** 610 mm<br/>
**Máxima Profundidad de Corte:** 90º<br/>
"

---

{{< gallery >}} 