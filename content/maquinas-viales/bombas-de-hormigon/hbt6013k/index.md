---
title: "Bombas de hormigón - HBT6013K"
layout: "single-vial"
draft: false

modelo: "HBT6013K"
medidas: "Medidas: 6100x2100x2810mm"
peso: "Peso Operativo: 7000Kg"
detalles: "
**Ap. Bomba:** 40-65m3/h<br/>
**Máxima Presión:** 18/13Mpa<br/>
**Capacidad de Bombeo:**<br/>
150m Vertical<br/>
600m Lineal"

---

{{< gallery >}} 