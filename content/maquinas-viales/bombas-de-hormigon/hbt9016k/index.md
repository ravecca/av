---
title: "Bombas de hormigón - HBT9016K"
layout: "single-vial"
draft: false

modelo: "HBT9016K"
medidas: "6482x2100x2886mm"
peso: "Peso Operativo: 7400Kg"
detalles: "
**Ap. Bomba:** 60-115m3/h<br/>
**Máxima Presión:** 9/18Mpa<br/>
**Capacidad de Bombeo:**<br/>
250m Vertical<br/>
1000m Lineal"
---

{{< gallery >}} 