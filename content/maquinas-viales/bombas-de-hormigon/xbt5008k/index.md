---
title: "Bombas de hormigón - XBT5008K"
layout: "single-vial"
draft: false

modelo: "XBT5008K"
medidas: "5623x1884x2606mm"
peso: "Peso Operativo: 5035Kg"
detalles: "
**Ap. Bomba:** 50m3/h<br/>
**Máxima Presión:** 8Mpa<br/>
**Capacidad de Bombeo:**<br/>
100m Vertical<br/>
400m Lineal"
---

{{< gallery >}} 