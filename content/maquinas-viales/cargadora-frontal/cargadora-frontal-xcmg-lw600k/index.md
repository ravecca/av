---
title: "Cargadora Frontal - XCMG LW600K"
layout: "single-vial"
draft: false

modelo: "XCMG LW600K"
medidas:
peso: "Peso Operativo: 20000 kg"
detalles: "
**Modelo del motor:** Cummins QSC8.3<br/>
**Potencia de motor:** 240 Hp  2200 rpm<br/>
**Radio mínimo de giro:** 6005 mm<br/>
**Máx. fuerza de arrastre:** 171 kN<br/>
**Máx. fuerza de arranque:** 205 kN<br/>
**Altura de descarga:** 4110 mm<br/>
**Alcance de descarga:** 1010 mm<br/>
**Carga nominal:** 6000 kg<br/>
**Capacidad del balde:** 3.5 m3<br/>
**Ancho total:** 3220 mm<br/>
**Alto total:** 3515 mm<br/>
**Largo total:** 8505 mm
"

---

{{< gallery >}} 