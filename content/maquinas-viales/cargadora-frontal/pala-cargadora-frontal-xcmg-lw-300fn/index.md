---
title: "Pala Cargadora Frontal - XCMG LW300FN"
layout: "single-vial"
draft: false

modelo: "XCMG LW300FN"
medidas:
peso: "Peso Operativo: 10000 kg"
detalles: "
**Modelo del motor:** YUCHAI - YC6B125-T21<br/>
**Potencia de motor:** 125 Hp<br/>
**Radio mínimo de giro:** 16060 mm<br/>
**Máx. fuerza de arrastre:** 90 kN<br/>
**Máx. fuerza de arranque:** 120 kN<br/>
**Altura de descarga:** 2930 mm<br/>
**Alcance de descarga:** 1010 mm<br/>
**Carga nominal:** 3000 kg<br/>
**Capacidad del balde:** 1.8 / 2 m3<br/>
**Ancho total:** 2482 mm<br/>
**Alto total:** 3025 mm<br/>
**Largo total:** 7050 mm
"

---

{{< gallery >}} 