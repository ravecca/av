---
title: "Cargadora Frontal - XCMG LZ50BR"
layout: "single-vial"
draft: false

modelo: "XCMG LZ50BR"
medidas:
peso: "Peso Operativo: 18000 kg"
detalles: "
**Modelo del motor:** WEICHAI - WD10G22 TURBO<br/>
**Potencia de motor:** 217 hp<br/>
**Radio mínimo de giro:** 6700 mm<br/>
**Máx. fuerza de arrastre:** 145 kN<br/>
**Máx. fuerza de arranque:** 170 kN<br/>
**Alcance de descarga:** 3090 mm<br/>
**Altura de descarga:** 4110 mm<br/>
**Carga nominal:** 5000 kg<br/>
**Capacidad del balde:** 3 m3<br/>
**Ancho total:** 2800 mm<br/>
**Alto total:** 3508 mm<br/>
**Largo total:** 8110 mm
"

---

{{< gallery >}} 