---
title: "Cargadora Frontal - XCMG LW300KN"
layout: "single-vial"
draft: false

modelo: "XCMG LW300KN"
medidas:
peso: "Peso Operativo: 10000 kg"
detalles: "
**Modelo del motor:** WEICHAI - WP6G125E22 - TURBO<br/>
**Potencia de motor:** 125 Hp<br/>
**Radio mínimo de giro:** 6020 mm<br/>
**Máx. fuerza de arrastre:** 100 kN<br/>
**Máx. fuerza de arranque:** 130 kN<br/>
**Altura de descarga:** 2930 mm<br/>
**Alcance de descarga:** 1010 mm<br/>
**Carga nominal:** 3000 kg<br/>
**Capacidad del balde:** 1.9 / 2 m3<br/>
**Ancho total:** 2482 mm<br/>
**Alto total:** 3290 mm<br/>
**Largo total:** 7250 mm
"

---

{{< gallery >}} 