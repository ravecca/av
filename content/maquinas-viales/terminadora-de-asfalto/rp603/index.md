---
title: "Terminadoras de asfalto RP"
layout: "single-vial"
draft: false

modelo: 
medidas:
peso: 
detalles: "
**RP603**<br/>
**Potencia Nominal:** 129Kw/170Hp@2200rpm<br/>
**Peso Operativo:** 18500Kg<br/>
**Ancho de Trabajo:** 2500-6000mm<br/>
**Ancho de Transporte:** 2500mm<br/>
**Medidas:** 6680x2500x3180<br/><br/>
**RP803**<br/>
**Potencia Nominal:** 140Kw/188Hp@2100rpm<br/>
**Peso Operativo:** 23300Kg<br/>
**Ancho de Trabajo:** 3000-8000mm<br/>
**Ancho de Transporte:** 2880mm<br/>
**Medidas:** 6680x3000x4000<br/><br/>
**RP903**<br/>
**Potencia Nominal:** 162KW/217Hp@2000rpm<br/>
**Peso Operativo:** 28820Kg<br/>
**Ancho de Trabajo:** 3000-9500mm<br/>
**Ancho de Transporte:** 3020mm<br/>
**Medidas:** 7320x3020x3950(3390)<br/><br/>
**RP1203**<br/>
**Potencia Nominal:** 191Kw/256Hp@2000rpm<br/>
**Peso Operativo:** 35500Kg<br/>
**Ancho de Trabajo:** 3000-12500mm<br/>
**Ancho de Transporte:** 2780mm<br/>
**Medidas:** 6680x3000x3950
"

---

{{< gallery >}} 