---
title: "Plataforma articulada GKH30"
layout: "single-vial"
draft: false

modelo: "Plataforma articulada GKH30"
medidas:
peso: 
detalles: "
**Longitud Total:** 13000 mm<br/>
**Ancho Total:** 2490/3590 mm<br/>
**Altura Total:** 2910 mm<br/>
**Distancia de Base de ejes:** 3600 mm<br/>
**Altura de Funcionamiento Nominal:** 30 m<br/>
**Máxima Altura de Plataforma:** 28.3 m<br/>
**Máxima Amplitud de Funcionamiento:** 24.3 m<br/>
**Grados de Amplitud Variable en Brazo:**:**12+80º<br/>
**Grados de Cambio de Amplitud en Braz0:**:**65+70º<br/>
"

---

{{< gallery >}} 