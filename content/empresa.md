---
title: "Empresa"
layout: "about"
draft: false

# who_we_are
who_we_are:
  enable: true
  subtitle: "Quienes somos"
  title: "American Vial Arrecifes"
  description: "Descripcion de la empresa American Vial Arrecifes, la mejor opción para alquilar máquinas viales y acceder a servicio técnico especializado."

  image: "images/about/american-vial-arrecifes.jpg"

# what_we_do
what_we_do:
  enable: true
  subtitle: "Nuestro trabajo"
  title: "Qué hacemos?"
  block:
  - title: "Un nuevo servicio"
    content: "Decade of engineering under his belt, Jeremie is responsible for technical infrastructure and feature development. In Flow, wherever things just work is understanding developing complex systems"

  - title: "Técnicos especializados"
    content: "Decade of engineering under his belt, Jeremie is responsible for technical infrastructure and feature development. In Flow, wherever things just work is understanding developing complex systems"
    
  - title: "Financiación adecuada"
    content: "Decade of engineering under his belt, Jeremie is responsible for technical infrastructure and feature development. In Flow, wherever things just work is understanding developing complex systems"
    
  - title: "Servicio Técnico"
    content: "Decade of engineering under his belt, Jeremie is responsible for technical infrastructure and feature development. In Flow, wherever things just work is understanding developing complex systems"

# our_mission
our_mission:
  enable: true
  subtitle: "NUESTRA MISIÓN"
  title: "Principal misión de nuestra empresa"
  description: "Proveer un servicio de excelencia para nuestros clientes y a la vez, otorgar el servicio técnico adecuado para máquinas viales."

  image: "images/about/american-vial.jpg"

# about_video
about_video:
  enable: true
  subtitle: "American Vial Arrecifes"
  title: "Un servicio único en alquiler de máquinas viales"
  description: "Protect your design vision and leave nothing up to interpretation with interaction recipes. Quickly share and access all your team members interactions by using libraries, ensuring consistcy throughout the."
  video_url: "https://www.youtube.com/embed/dyZcRRWiuuw"
  video_thumbnail: "images/about/american-vial-arrecifes.jpg"


# brands
brands_carousel:
  enable: true
  subtitle: "Nuestras Marcas"
  title: "Estas empresas nos apoyan"
  section: "/" # brand images comming form _index.md


# our team
# our_team:
#   enable: true
#   subtitle: "Our members"
#   title: "The People Behind"
#   description: "We were freelance designers and developers, constantly finding <br> ourselves deep in vague feedback. This made every client and team"
#   team:
#   - name: "Valentin Staykov"
#     image: "images/about/team/01.jpg"
#     designation: "Operations"
#   - name: "Bukiakta Bansalo"
#     image: "images/about/team/02.jpg"
#     designation: "Product"
#   - name: "Ortrin Okaster"
#     image: "images/about/team/03.jpg"
#     designation: "Engineering"


# our office
# our_office:
#   enable: true
#   subtitle: "Our Offices"
#   title: "Made with Love Of around the world With Many Offices"
#   description: "We were freelance designers and developers, constantly finding <br> ourselves deep in vague feedback. This made every client and team"
#   office_locations:
#   - city: "NewYork, USA"
#     country_flag: "images/about/flags/us.png"
#     address_line_one: "219 Bald Hill Drive"
#     address_line_two: "Oakland Gardens, NY 11364"
#   - city: "Australia, Perth"
#     country_flag: "images/about/flags/au.png"
#     address_line_one: "Flat 23 80 Anthony Circlet"
#     address_line_two: "Port Guiseppe, TAS 2691"
#   - city: "Berlin, Germany"
#     country_flag: "images/about/flags/germany.png"
#     address_line_one: "Jl Raya Dewi Sartika Ged"
#     address_line_two: "Harapan Masa, Br Germeny"
#   - city: "China, Wohan"
#     country_flag: "images/about/flags/china.png"
#     address_line_one: "1hao Wen Ti Huo Dong"
#     address_line_two: "Zhong Xin 1ceng Jian Xing"

---