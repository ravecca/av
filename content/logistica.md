---
title: "Maquinas para Logística - American Vial Arrecifes"
layout: "products-list"
draft: false
logistica: true

# our office
products:
  logistica: true
  enable: true
  subtitle: "Máquinas Logística"
  title: "Máquinas Logística"
  description: "Máquinas para Logística, equipos Logística en alquiler y venta."
  products_items:
  - name: "Autoelevadores 2 a 4,5 T"
    image: "images/maquinas-categorias/log/log_autoelevadores-2-45t.png"
    logistica: true
  - name: "Autoelevadores 5 a 10 T"
    image: "images/maquinas-categorias/log/log_autoelevadores-5-10t.png"
    logistica: true
  - name: "Autoelevadores 10 a 25 T"
    image: "images/maquinas-categorias/log/log_autoelevadores-10-25t.png"
    logistica: true
  - name: "Manipuladores Magni"
    image: "images/maquinas-categorias/log/log_manipuladores-magni.png"
    logistica: true
  - name: "Porta Contenedor"
    image: "images/maquinas-categorias/log/log_portacontenedor.png"
    logistica: true

---