---
title: "Mini cargadoras - American Vial Arrecifes"
layout: "products-list-vial"
draft: false

products:
  enable: true
  subtitle: "Mini cargadoras"
  title: "Vial Mini cargadoras"
  description: "Mini cargadoras, equipos viales en alquiler y venta."
  products_items:
  - name: "Mini cargadora WECAN XT650"
    image: "images/maquinas-categorias/vial/vial_mini-cargadoras.png"
    link: "./maquinas-viales/mini-cargadoras/minicargadora-wecan-xt650/"
  - name: "Mini cargadora WECAN 1100"
    image: "images/maquinas-categorias/vial/vial_mini-cargadoras.png"
    link: "./maquinas-viales/mini-cargadoras/minicargadora-wecan-1100/"
---