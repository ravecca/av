---
title: "Tractores Serie Compacta - American Vial Arrecifes"
layout: "products-list-agro"
draft: false

# our office
products:
  enable: true
  subtitle: "Tractores Serie Compacta"
  title: "Tractores Serie Compacta"
  description: "Tractores Serie Compacta, equipos agropecuarios en alquiler y venta."
  products_items:
  - name: "Agricolas RD 704 Compacta"
    image: "images/maquinas-categorias/agro/serie_compacta/agro_tractores_agricolas_rd704_compacta.png"
    link: "./tractores-agro/serie-compacta/rd704/"
  - name: "Viñateros RD 504 Compacta"
    image: "images/maquinas-categorias/agro/serie_compacta/agro_tractores_vinatores_rd504_compacta.png"
    link: "./tractores-agro/serie-compacta/rd504/"

---