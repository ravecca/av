---
# banner
banner:
  title: "El poder de una marca. La garantía del líder."
  button: "Contactar ahora"
  button_link: ""
  image: "images/excavadora.png"


# brands
brands_carousel:
  enable: true
  brand_images:
  - "images/brands/chery.png"
  - "images/brands/wecan.png"
  - "images/brands/xcmg.png"
  - "images/brands/chery.png"
  - "images/brands/wecan.png"
  - "images/brands/xcmg.png"

# categories
categories:
  cat_sections:
    - image: "images/main-agro.png"
      link: "agro"
    - image: "images/main-logistica.png"
      link: "logistica" 
    - image: "images/main-vial.png"
      link: "vial"

# features
features:
  enable: true
  subtitle: "American Vial"
  title: "Servicios <br> para el cliente"
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi egestas <br> Werat viverra id et aliquet. vulputate egestas sollicitudin."
  features_blocks:
  - icon: "las la-lock"
    title: "Seguridad total"
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Neque enim id diam ornare volutpat in sagitis, aliquet. Arcu cursus"
  - icon: "las la-magnet"
    title: "Servicio Técnico"
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Neque enim id diam ornare volutpat in sagitis, aliquet. Arcu cursus"
  - icon: "las la-tachometer-alt"
    title: "Asesoramiento especializado"
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Neque enim id diam ornare volutpat in sagitis, aliquet. Arcu cursus"
  - icon: "las la-link"
    title: "Financiamiento"
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Neque enim id diam ornare volutpat in sagitis, aliquet. Arcu cursus"



# intro_video
intro_video:   
  enable: true
  subtitle: "American Vial Arrecifes"
  title: "Equipos viales en venta y alquiler"
  description: "En nuestro punto de venta de Arrecifes Ud contara con el asesoramiento adecuado para su necesidad."
  video_url: "http://c2181469.ferozo.com/test/wp-content/uploads/2021/04/WhatsApp-Video-2021-04-19-at-15.49.42.mp4"
  video_thumbnail: "images/about/american-vial.jpg"


# how_it_works
how_it_works:   
  enable: true
  block:
  - subtitle: "Motoniveladoras"
    title: "Motoniveladoras"
    description: "Contamos con máquinas motoniveladoras para construcción vial. Amplia variedad de potencias y performances."
    image: "images/maquinas-categorias/motoniveladoras.png"

  - subtitle: "Retroexcavadoras"
    title: "Retroexcavadoras"
    description: "Contamos con máquinas motoniveladoras para construcción vial. Amplia variedad de potencias y performances."
    image: "images/maquinas-categorias/excavadoras.png"


# testimonials
# testimonials:   
#   enable: true
#   subtitle: "Opiniones de clientes"
#   title: "Nuestros clientes nos recomiendan"
#   description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi egestas <br> Werat viverra id et aliquet. vulputate egestas sollicitudin."
#   image_left: "images/testimonials-01.png"
#   image_right: "images/testimonials-02.png"
  
  # testimonials_quotes:
  # - quote: "Lorem ipsum dolor amet, conseetur adipiscing elit. Ornare quam porta arcu congue felis volutpat. Vitae lectudbfs dolor faucibus"
  #   name: "David Cameron"
  #   designation: "CEO, Nexuspay"
  #   image: "images/user-img/05-i.jpg"

  # - quote: "Conseetur adipiscing elit. Ornare quam porta arcu congue felis volutpat. Vitae lectudbfs pellentesque vitae dolor faucibus"
  #   name: "David Cameron"
  #   designation: "CEO, Nexuspay"
  #   image: "images/user-img/06-i.jpg"

  # - quote: "Lorem ipsum dolor amet, conseetur adipiscing elit. Ornare quam porta arcu congue felis volutpat. Vitae lectudbfs pellentesque vitae dolor"
  #   name: "David Cameron"
  #   designation: "CEO, Nexuspay"
  #   image: "images/user-img/07-i.jpg"

  # - quote: "Lorem ipsum dolor amet, conseetur adipiscing elit. porta arcu congue felis volutpat. Vitae lectudbfs pellentesque vitae dolor faucibus"
  #   name: "David Cameron"
  #   designation: "CEO, Nexuspay"
  #   image: "images/user-img/08-i.jpg"

  # - quote: "Lorem ipsum dolor ame conseetur. Ornare quam porta arcu congue felis volutpat. Vitae lectudbfs pellentesque vitae dolor faucibus"
  #   name: "David Cameron"
  #   designation: "CEO, Nexuspay"
  #   image: "images/user-img/09-i.jpg"

  # - quote: "Lorem ipsum dolor amet, conseetur adipiscing elit. Ornare quam porta arcu congue lectudbfs pellentesque vitae dolor faucibus"
  #   name: "David Cameron"
  #   designation: "CEO, Nexuspay"
  #   image: "images/user-img/10-i.jpg"

---
