---
title: "Tractores Serie RK - American Vial Arrecifes"
layout: "products-list-agro"
draft: false

# our office
products:
  enable: true
  subtitle: "Tractores Serie RK"
  title: "Tractores Serie RK"
  description: "Tractores Serie RK, equipos agropecuarios en alquiler y venta."
  products_items:
  - name: "Agrícolas RK 604 Yerbatero"
    image: "images/maquinas-categorias/agro/serie_rk/agro_tractores_agricolas_rk-604_yerbatero.png"
    link: "./tractores-agro/serie-rk/rk604y/"
  - name: "Agrícolas RK 750 - 754"
    image: "images/maquinas-categorias/agro/serie_rk/agro_tractores_agricolas_rk-750 -754.png"
    link: "./tractores-agro/serie-rk/rk750-754/"
  - name: "Agrícolas RK 904"
    image: "images/maquinas-categorias/agro/serie_rk/agro_tractores_agricolas_rk-904.png"
    link: "./tractores-agro/serie-rk/rk904/"
  - name: "Frutero RK 504"
    image: "images/maquinas-categorias/agro/serie_rk/agro_tractores_frutero_rk-504.png"
    link: "./tractores-agro/serie-rk/rk504f"
---