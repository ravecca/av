---
title: "Plataformas articuladas - American Vial Arrecifes"
layout: "products-list-vial"
draft: false

products:
  enable: true
  subtitle: "Plataformas articuladas"
  title: "Vial Plataformas articuladas"
  description: "Plataformas articuladas, equipos viales en alquiler y venta."
  products_items:
  - name: "Plataforma articulada GKH30"
    image: "images/maquinas-categorias/vial/vial_plataformas-articuladas.png"
    link: "./maquinas-viales/plataforma-articuladas/gkh30/"
    
---