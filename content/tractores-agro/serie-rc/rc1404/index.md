---
title: "RC 1404 - American Vial Arrecifes"
layout: "single"
draft: false

traccion: "Doble 4x4"
motortipo: "Diesel 4 cilidros, 4 tiempos, inyeccion directa. Refrigerado por agua, tecnología Perkins."
potencia: "140 hp"
desplazamiento: "4070 cm3"
velocidadrotacion: "2300 rpm"
marchas: "24 de avance / 24 de retroceso"
velocidad: "min-max (avance / retroceso) 2,19 - 29,41 / 4,72 - 13,47 Km/h"
freno: "Disco en baño de aceite"
fuerza: "(PTO / rpm)540 / 1000"
suspension: "suspensión de 3 puntos Cat. 3"
levante: "Capacidad de levante a 610mm 1733 kg"
largo: "Largo total 4382 mm"
ancho: "Ancho total 2083 mm"
altitud: "Altitud total 2760 mm"
peso: "Peso 4500 Kg"

---

{{< gallery >}} 