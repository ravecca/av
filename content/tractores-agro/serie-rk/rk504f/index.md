---
title: "RK504 - Frutero - American Vial Arrecifes"
layout: "single"
draft: false

traccion: "Doble 4x4"
motortipo: "Diesel 4 cilindros, 4 tiempos, inyección directa. 498BT. Refriegerado por agua"
potencia: "58 hp"
desplazamiento: "3168 cm3"
velocidadrotacion: "2400 rpm"
marchas: "12 de avance / 12 de retroceso"
velocidad: "min-max (avance / retroceso) 2,35 - 33 / 2,06 - 27 Km/h"
freno: "Disco seco"
fuerza: "(PTO / rpm) 540 / 1000"
suspension: "suspensión de 3 puntos Cat. 2"
levante: "Capacidad de levante a 610mm 833 kg"
largo: "Largo total 3980 mm"
ancho: "Ancho total 1650 mm"
altitud: "Altitud total 1400 mm. Min. distancia al suelo 325 mm"
peso: "Peso 2170Kg"

---

{{< gallery >}} 