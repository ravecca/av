RK504 - Frutero
DETALLES TÉCNICOS
Tracción RK504Doble 4x4
Motor tipoDiesel 4 cilindros, 4 tiempos, inyección directa. 498BT. Refriegerado por agua
Potencia58 hp
Desplazamiento3168 cm3
Velocidad de rotación2400 rpm
Número de marchas12 de avance / 12 de retroceso
Velocidad min-max (avance / retroceso)2,35 - 33 / 2,06 - 27 Km/h
Freno principalDisco seco
Toma de fuerza (PTO / rpm)540 / 1000
Suspensión de 3 puntosCat. 2
Capacidad de levante a 610mm833 kg
Largo total3980 mm
Ancho total1650 mm
Altitud total1400 mm
Min. distancia al suelo325 mm
Peso2170 Kg