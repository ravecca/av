---
title: "RD404 Estrecho Viñatero - American Vial Arrecifes"
layout: "single"
draft: false

traccion: "Simple 4x2 / Doble 4x4"
motortipo: "Diesel 4 cilindros, 4 tiempos, inyeccion directa. 498BT. Refriegerado por agua. Motor Perkins"
potencia: "45 hp"
desplazamiento: "1833 cm3"
velocidadrotacion: "2350 rpm"
marchas: "8 de avance / 2 de retroceso"
velocidad: "min-max (avance / retroceso) 1,71 - 26 / 2,05 - 10,20 Km/h"
freno: "Disco seco"
fuerza: "(PTO / rpm) 540 / 1000"
suspension: "suspensión de 3 puntos Cat. 1"
levante: "Capacidad de levante a 610mm 615 kg"
largo: "Largo total 3100 mm"
ancho: "Ancho total 1400 mm"
altitud: "Altitud total 1500 mm"
peso: "Peso 1700"

---

{{< gallery >}} 