---
title: "RD300P / 304P - American Vial Arrecifes"
layout: "single"
draft: false

traccion: "RD300: Simple 4x2 - RD304: Doble 4x4"
motortipo: "Diesel 3 cilindros, 4 tiempos, inyeccion directa. KM358BT. Refriegerado por agua"
potencia: "30 hp"
desplazamiento: "1533 cm3"
velocidadrotacion: "2350 rpm"
marchas: "8 de avance / 2 de retroceso"
velocidad: "min-max (avance / retroceso) 1,71 - 26 / 2,05 - 7,20 Km/h"
freno: "Disco seco"
fuerza: "(PTO / rpm) 540 / 1000"
suspension: "suspensión de 3 puntos Cat. 1"
levante: "Capacidad de levante a 610mm 433 kg"
largo: "Largo total 3225 mm"
ancho: "Ancho total 1440 mm"
altitud: "Altitud total 2420 mm"
peso: "Peso RD300: 1190 Kg - Peso RD304: 1230Kg"

---

{{< gallery >}} 