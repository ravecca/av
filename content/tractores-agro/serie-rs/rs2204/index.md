---
title: "RS 2204 - American Vial Arrecifes"
layout: "single"
draft: false

traccion: "Doble 4x4"
motortipo: "Diesel 6 cilidros, 4 tiempos, inyeccion directa. Refrigerado por agua, tecnología Perkins TURBO."
potencia: "220 hp TURBO"
desplazamiento: "7600 cm3"
velocidadrotacion: ""
marchas: "32 de avance / 32 de retroceso"
velocidad: "min-max (avance / retroceso) 1.19-34.41 / 2.72-19.47 Km/h"
freno: "Disco en baño de aceite"
fuerza: "(PTO / rpm) 540 / 1000"
suspension: "suspensión de 3 puntos Cat. 3"
levante: "Capacidad de levante a 610mm 3500 kg"
largo: "Largo total 5300 mm"
ancho: "Ancho total 3600 mm"
altitud: "Altitud total 3354 mm. Min distancia al suelo 450 mm"
peso: "Peso 9000 Kg"

---

{{< gallery >}} 