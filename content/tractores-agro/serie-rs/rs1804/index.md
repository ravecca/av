---
title: "RS 1804 - American Vial Arrecifes"
layout: "single"
draft: false

traccion: "Doble 4x4"
motortipo: "Diesel 6 cilidros, 4 tiempos, inyeccion directa. Refrigerado por agua, tecnología Perkins."
potencia: "180 hp TURBO"
desplazamiento: "5800 cm3"
velocidadrotacion: ""
marchas: "24 de avance / 8 de retroceso"
velocidad: "min-max (avance / retroceso) 1.61-32.41 / 1.77-20.47 Km/h"
freno: "Disco seco"
fuerza: "(PTO / rpm) 540 / 1000"
suspension: "suspensión de 3 puntos Cat. 3"
levante: "Capacidad de levante a 610mm 2200 kg"
largo: "Largo total 5060 mm"
ancho: "Ancho total 2265 mm"
altitud: "Altitud total 2995 mm. Min distancia al suelo 470 mm"
detalle: "Trocha delantera 1900/1810 ajustable. Trocha trasera 1816/2410 ajustable"
peso: "Peso 3800 Kg"

---

{{< gallery >}} 