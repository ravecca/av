---
title: "Serie Compacta RD 704 - American Vial Arrecifes"
layout: "single"
draft: false

traccion: "Simple 4x2"
motortipo: "Perkins, Diesel 4 cilindros, 4 tiempos, inyeccion directa. Refriegerado por agua"
potencia: "80 hp"
desplazamiento: "4500 cm3"
# velocidadrotacion: "2350 rpm"
marchas: "12 de avance / 12 de retroceso. Inversor mecánico"
velocidad: "min-max (avance / retroceso) 2,15 - 28,66 / 1,89 - 25,13 Km/h"
freno: "A disco con baño de aceite"
fuerza: (PTO / rpm)540 / 1000
suspension: "suspensión de 3 puntos Cat. 2"
levante: "Capacidad de levante a 610mm 1450 kg"
largo: "Largo total 4030 mm"
ancho: "Ancho total 1650 mm"
altitud: "Altitud total 2250 mm"
peso: "Peso 2270 Kg"

---

{{< gallery >}} 