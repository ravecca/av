---
title: "RA 504 - American Vial Arrecifes"
layout: "single"
draft: false

traccion: "Simple 4x4"
motortipo: "Diesel 4 cilindros, inyección Directa, bomba Lineal Refrigerado por agua. Aspiración Natural"
potencia: "58 hp"
desplazamiento: "1255 cm3"
velocidadrotacion: "3100 rpm"
marchas: "8 de avance / 2 de retroceso"
velocidad: "min-max (avance / retroceso) 2,35 - 33 / 2,06 - 27 Km/h"
freno: "Disco seco"
fuerza: "(PTO / rpm)540 / 1000"
suspension: "suspensión de 3 puntos Cat. 1"
levante: "Capacidad de levante a 610mm 720 kg"
largo: "Largo total 3412 mm"
ancho: "Ancho total 1470 mm"
altitud: "Altitud total 2420 mm"
peso: "Peso 1700 Kg"

---

{{< gallery >}} 