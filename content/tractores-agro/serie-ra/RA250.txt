RA250
DETALLES TÉCNICOS
TracciónSimple 4x2
Motor tipoDiesel 3 cilindros, 4 tiempos, inyeccion directa. KM358BT. Refrigerado por agua
Potencia25 hp
Desplazamiento1255 cm3
Velocidad de rotación2350 rpm
Número de marchas6 de avance / 1 de retroceso
Velocidad min-max (avance / retroceso)1,71 - 26 / 2,05 - 7,20 Km/h
Freno principalDisco seco
Toma de fuerza (PTO / rpm)540 / 1000
Suspensión de 3 puntosCat. 0
Capacidad de levante a 610mm433 kg
Largo total2700 mm
Ancho total1440 mm
Altitud total1350 mm
Peso850 Kg
