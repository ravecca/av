---
title: "RA 250 - American Vial Arrecifes"
layout: "single"
draft: false

traccion: "Simple 4x2"
motortipo: "Diesel 3 cilindros, 4 tiempos, inyeccion directa. KM358BT. Refrigerado por agua"
potencia: "25 hp"
desplazamiento: "1255 cm3"
velocidadrotacion: "2350 rpm"
marchas: "6 de avance / 1 de retroceso"
velocidad: "min-max (avance / retroceso) 1,71 - 26 / 2,05 - 7,20 Km/h"
freno: "Disco seco"
fuerza: "(PTO / rpm)540 / 1000"
suspension: "suspensión de 3 puntos Cat. 0"
levante: "Capacidad de levante a 610mm 433 kg"
largo: "Largo total 2700 mm"
ancho: "Ancho total 1440 mm"
altitud: "Altitud total 1350 mm"
peso: "Peso 850 Kg"

---

{{< gallery >}} 