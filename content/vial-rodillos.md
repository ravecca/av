---
title: "Rodillos compactadores - American Vial Arrecifes"
layout: "products-list-vial"
draft: false

products:
  enable: true
  subtitle: "Rodillos compactadores"
  title: "Vial Rodillos compactadores"
  description: "Rodillos compactadores, equipos viales en alquiler y venta."
  products_items:
  - name: "Rodillos compactadores XS"
    image: "images/maquinas-categorias/vial/vial_rodillos.png"
    link: "./maquinas-viales/rodillos/rodillo-compactador/"
  - name: "Rodillos compactadores XD"
    image: "images/maquinas-categorias/vial/vial_rodillos.png"
    link: "./maquinas-viales/rodillos/rodillo-compactador-doble-liso/"
  - name: "Rodillos compactadores neumaticos"
    image: "images/maquinas-categorias/vial/vial_rodillos.png"
    link: "./maquinas-viales/rodillos/rodillo-neumatico/"
  - name: "Rodillos compactadores mini"
    image: "images/maquinas-categorias/vial/vial_rodillos.png"
    link: "./maquinas-viales/rodillos/xmr153-mini-rodillo-compactador/"
  - name: "Rodillos compactadores XMR203"
    image: "images/maquinas-categorias/vial/vial_rodillos.png"
    link: "./maquinas-viales/rodillos/xmr203/"

---