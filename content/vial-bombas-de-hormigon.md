---
title: "Vial Bombas de Hormigón - American Vial Arrecifes"
layout: "products-list-vial"
draft: false

products:
  enable: true
  subtitle: "Bombas de Hormigón"
  title: "Vial Bombas de Hormigón"
  description: "Bombas de Hormigón, equipos viales en alquiler y venta."
  products_items:
  - name: "Bombas de Hormigón XBT5008K"
    image: "images/maquinas-categorias/vial/vial_bombas-de-hormigon.png"
    link: "./maquinas-viales/bombas-de-hormigon/xbt5008k/"
  - name: "Bombas de Hormigón HBT6013K"
    image: "images/maquinas-categorias/vial/vial_bombas-de-hormigon.png"
    link: "./maquinas-viales/bombas-de-hormigon/hbt6013k/"
  - name: "Bombas de Hormigón HBT9016K"
    image: "images/maquinas-categorias/vial/vial_bombas-de-hormigon.png"
    link: "./maquinas-viales/bombas-de-hormigon/hbt9016k/"
---